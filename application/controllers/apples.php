<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apples extends CI_Controller {

    public function index()
    {
        $this->load->model('apples_model');
        $data['apples'] = $this->apples_model->get_apples();
        $this->load->view('apples_view',$data);
    }

    public function add_apples()
    {
        $this->load->library('form_validation');
        if (isset ($_POST['add'])){
            $this->load->model('rules_model');;
            $this->form_validation->set_rules($this->rules_model->add_rules);
            $check = $this->form_validation->run();
            echo $check;
            if ($check == TRUE){
                $data['sort'] = $this->input->post('sort');
                $data['quantity'] = $this->input->post('quantity');
                $this->load->model('apples_model');
                $this->apples_model->add_apples($data);
                $this->load->view('add_info_view');
            }
            else{
                $this->load->view('add_apples_view');
            }
        }
        else{
            $this->load->view('add_apples_view');
        }
    }

    public function edit_apples()
    {
        $this->load->library('form_validation');
        if (isset ($_POST['edit'])){
            $this->load->model('rules_model');;
            $this->form_validation->set_rules($this->rules_model->edit_rules);
            $check = $this->form_validation->run();
            if ($check == TRUE){
                $id = $this->input->post('id');
                $data['sort'] = $this->input->post('sort');
                $data['quantity'] = $this->input->post('quantity');
                $this->load->model('apples_model');
                $this->apples_model->edit_apples($data, $id);
                $this->load->view('edit_info_view');
            }
            else{
                $this->load->view('edit_apples_view');
            }
        }
        else{
            $this->load->view('edit_apples_view');
        }
    }

    public function del_apples()
    {
        $this->load->library('form_validation');
        if (isset ($_POST['del'])){
            $this->load->model('rules_model');;
            $this->form_validation->set_rules($this->rules_model->del_rules);
            $check = $this->form_validation->run();
            if ($check == TRUE){
                $id = $this->input->post('id');
                $this->load->model('apples_model');
                $this->apples_model->del_apples($id);
                $this->load->view('del_info_view');
            }
            else{
                $this->load->view('del_apples_view');
            }
        }
        else{
            $this->load->view('del_apples_view');
        }
    }

}
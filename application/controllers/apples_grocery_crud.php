<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apples_grocery_crud extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->helper('url');

        $this->load->library('grocery_CRUD');
    }

    public function _example_output($output = null)
    {
        $this->load->view('apples_grocery_crud_view.php', $output);
    }

    public function offices()
    {
        $output = $this->grocery_crud->render();

        $this->_example_output($output);
    }

    public function index()
    {
        $this->_example_output((object)array('output' => '', 'js_files' => array(), 'css_files' => array()));
    }

    public function apples()
    {
        try {
            $crud = new grocery_CRUD();
            $crud->set_table('apples');
            $crud->required_fields('id', 'sort', 'quantity');
            $crud->columns('id', 'sort', 'quantity');
            $crud->set_rules('sort', 'Сорт', 'required|xss_clean|alpha|max_length[30]|trim');
            $crud->set_rules('quantity', 'Количество', 'required|xss_clean|integer|trim');
            $output = $crud->render();

            $this->_example_output($output);

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
    }

}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apples_model extends CI_Model {

	public function get_apples()
	{
        $query = $this->db->get('apples');
        return $query->result_array();
	}

    public function add_apples($data)
    {
        $this->db->insert('apples',$data);
    }

    public function edit_apples($data, $id)
    {
        $this->db->where('id',$id);
        $this->db->update('apples',$data);
    }

    public function del_apples($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('apples');
    }
}

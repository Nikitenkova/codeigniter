<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rules_model extends CI_Model {

	public $add_rules = array(

        array(
            'field' => 'sort',
            'label' => 'Сорт',
            'rules' => 'required|xss_clean|alpha|max_length[30]|trim',
        ),
        array(
            'field' => 'quantity',
            'label' => 'Количество',
            'rules' => 'required|xss_clean|integer|trim',
        ),
    );

    public $edit_rules = array(

        array(
            'field' => 'sort',
            'label' => 'Сорт',
            'rules' => 'required|xss_clean|alpha|max_length[30]|trim',
        ),
        array(
            'field' => 'quantity',
            'label' => 'Количество',
            'rules' => 'required|xss_clean|integer|trim',
        ),
        array(
            'field' => 'id',
            'label' => 'id',
            'rules' => 'required|xss_clean|integer|trim',
        ),
    );

    public $del_rules = array(

        array(
            'field' => 'id',
            'label' => 'id',
            'rules' => 'required|xss_clean|integer|trim',
        ),
    );
}
